# Android App

<img src="./Screenshots/Main.png" height="500px">

## Initial setup

### Device administrator permissions

When the user installs the app, they are first prompted to allow the app to become a device administrator.

By granting the app those permissions, the user allows the app to reset the device back to factory setting and deleting all of the user's data on the phone in the process.

This is an optional permission that the user can grant or deny, depending on their preferences.

The app asks for this level of privileges because this will allow the user to factory reset their own device, once they lose it and determine that they are unable to get it back. In such a case, the user may decide that they want to remotely wipe their device to ensure that no unauthorized third party is going to get access to their private and sensitive data.

Note: It's currently uncertain, if factory reset requests actually work. While testing in an emulator, a few debugging logs appeared and nothing else happened. The author didn't have the chance to test it on real hardware yet.

### Device registration

After the user has granted or denied the app to become a device administrator, they are greeted by a setup screen that asks them for an address and a password.

<img src="./Screenshots/Setup.png" height="500px">

The address field is the URL of the Find My Device server that the user wants to use.

Once the user hits the submit button, the app is going to register the device on the server. By doing that, the Android app sends a HTTP post request to the server's /register endpoint with an empty HTTP body. The server then returns a UUID, that is now going to identify the device.

After the new UUID has been successfully retrieved, the app will then show the user a screen that tells, that they are going to be prompted for a file location to which important backup information is going to be stored to. It also tells the user to copy this file to a safe location so that they still have access to it, when they do end up losing their device.

<img src="./Screenshots/Backup.png" height="500px">

### Location permission request

After the user has registered their device, the app will ask the user to allow it to access the location information of the device both in the foreground and in the background. This is needed if the user wants to be able to find their device again in the case the device goes missing.

<img src="./Screenshots/Permission.png" height="500px">

## Technical overview

The Find My Device Android app has been created using the Android Studio IDE using the Kotlin programming language. This was chosen because it's the recommended way to develop native Android apps with direct access to all Android features, some of which may not be easily accessible with a web app-based approach. Because of this and the fact that there currently isn't really a comparable custom ROM solution for iOS devices anyway, this seemed like the most straightforward and logical decision to make. The author of this app is also already familiar with Android app development, even if his experience dates back several years.


### End-to-end encryption: MAC and encrypt

Whenever data gets end-to-end encrypted, the following steps are performed:
1. A MAC of the input data is generated using the HmacSHA256 algorithm with the user's cryptographic key, to secure the data against tampering.
2. The input data along with the MAC and the used cryptographic algorithm and cryptographic parameters get serialized and encoded as a JSON object. Both the MAC and the data belonging to the MAC get Base64 encoded in the process.
3. This JSON object gets encrypted using the AES algorithm in CBC mode with the key length of 256 and PKCS5PADDING. The key used here is the same as in step 2.
4. The encrypted JSON object and the cryptographic algorithm and parameters like IV get encoded in an enclosing JSON object.

### Backup file

Once the user has selected a location and filename for their backup file, the backup file gets generated and saved to the previously selected location.

The backup file generation works as follows:
1. A 512 bit long random salt is generated using the SecureRandom class from the Android API.
2. A 256 bit long cryptographic key gets derived using the previously chosen password and salt using the PBKDF2withHmacSHA256 algorithm with the iteration count set to 600'000, as recommended by [OWASP](https://cheatsheetseries.owasp.org/cheatsheets/Password_Storage_Cheat_Sheet.html#pbkdf2).
3. The UUID gets end-to-end encrypted using the previously described "MAC and encrypt" procedure.
8. The resulting JSON object gets saved to the backup file.

Note: because the key derivation failed with an empty password, the password is going to be set to the string "empty", if the user has decided not to supply any password at all.

The UUID gets encrypted because the user may choose to save their backup file in an untrusted environment such as a public cloud such as Google Drive while trying to hide as much potentially sensitive information as possible.

An actor that only has access to the UUID would be able to see which UUID belongs to which user and may be able to attempt to perform targeted attacks more easily.

### Location data submission

The Find My Device app collects the device's location once every hour and sends it to the server to store, to ensure that the user is able to find their device again in case it gets lost.

Before the location data gets sent to the server, the location data along with the relevant timestamps get end-to-end encrypted using the previously described "MAC and encrypt" procedure and then sent to the server.

### Remote command polling

The Find My Device app also sends requests to the server once every hour to check, if the user has sent any remote commands to it, such as a remote factory reset command. Currently this is the only implemented command, but this may be expanded in the future.

### Map: OpenStreetMap

Once the setup process has been completed, the app will show the user's current location in the app using the osmdroid library, which displays the location using OpenStreetMap. This option was preferred over alternatives like Google Maps because they have a better reputation when it comes to data collection concerns. In addition to this, Google Maps requires the app developer to create a Google account with a limited API quota and it would require the developer to embed the Google Maps SDK into the app itself unless he resorted to some WebView trickery instead. In the future, it may also be an option to download the relevant map data to the device with OpenStreetMaps to limit privacy related concerns even more.


### Important files

#### File: Crypto.kt

This file contains all functions and data classes that are related to the relevant cryptographic operations. Those include:

Used functions:
- genSalt
    - generates a random 512 bit long salt
- encrypt
    - encrypts arbitrary data with a cryptographic key using the AES algorithm in CBC mode with the key length of 256 and PKCS5PADDING
- decrypt
    - decrypts arbitrary data with a cryptographic key using the AES algorithm in CBC mode with the key length of 256 and PKCS5PADDING
- mac
    - created a MAC of arbitrary data with a cryptographic key using the HmacSHA256 algorithm
- genBackupFile
    - generates the backup file
- macAndEncrypt
    - creates a MAC of arbitrary data and encrypts it, as described in the "MAC and encrypt" segment
- deriveKey
    -  derives a 256 bit long cryptographic key from a password and salt using the PBKDF2withHmacSHA256 algorithm with the iteration count set to 600'000, as recommended by [OWASP](https://cheatsheetseries.owasp.org/cheatsheets/Password_Storage_Cheat_Sheet.html#pbkdf2)

Unused functions:
- genKey
    - generates a random 256 bit AES key
- genKeyPair
    - generates a 4096 bit RSA keypair
- sign
    - signs arbitrary data with a private key using SHA512withRSA
- verify
    - verifies the signature of arbitrary data with a public key using SHA512withRSA

Data classes:
- AuthenticatedData
    - a class that contains data, the MAC of that data and the cryptographic parameters that have been used to generate the MAC
- EncryptedData
    - a class that contains encrypted data and the cryptographic parameters that have been used during encryption
- BackupFile
    - a class that contains the authenticated and encrypted UUID of the device along with the PBKDF2 parameters that have been used


#### Package: activities

This package contains all the activities of the Android app. Activities are screens that the user can see and interact with. Currently there are three activities:
- SetupActivity
    - this is the initial registration screen
- SaveBackupActivity
    - this is the screen that tells the user to store their backup file in a safe location before prompting them for a file location
- PermissionActivity
    - on this activity, the user is going to be asked to allow the app to access the device's location information both in the foreground and in the background, so that the user can later find their device again in case the device goes missing.
- MainActivity
    - this is the activity that the user is going to see once everything has been setup, which is their current location on the map

#### Package: receivers

- AlarmReceiver
    - The receiver that gets called on an hourly basis, that requests commands from the server and submits location information to the server.
- BootBroadcastReceiver
    - The receiver that automatically gets called by the operating system, when the device gets rebooted. It sets up the hourly location and command request alarms.
- DeviceAdminReceiver
    - the receiver that's being called when administrative privileges have been granted to the app

#### Dependencies

##### NetCipher

NerCipher is a library that, among other things, allows developers to easily integrate Orbot into an Android app. Orbot is an application that allows other apps to route their internet traffic through the [Tor network](https://torproject.org).

The idea of including this in the Android app was to optionally route all of the collected, end-to-end encrypted location data through Tor before sending it to the backend server to hide the user's IP address. The reason for that is to hide as much personal information from the server as possible and in turn making the Find My Device experience as private as possible. By using Tor, the server wouldn't be able to guess the device's location at all anymore, not even on the basis of their IP address. This is not an issue for the user, since they are going to still be able to access their end-to-end encrypted location data regardless.

Since NetCipher wasn't able to detect Orbot on the developer's device, this functionality has not yet been implemented.

##### OkHttp3

OkHttp3 is the HTTP library that has been used for the Android app because it's easy to use and very flexible.

NetCipher also offers official support for OkHttp3 integration. This integration doesn't currently seem to be working with current versions of OkHttp3 however but since OkHttp3 already supports HTTP proxies, adding Tor support with OkHttp3 shouldn't be an issue when it comes to OkHttp3.


##### Google Play Services: Location

To retrieve the device's location Google Play Services's Location service is being used. This may be a controversial choice, given that the goal of this Find My Device system is to offer the best possible user privacy. It has been chosen however, because it claims to offer the best trade-off when it comes to saving on battery usage and geolocation precision. On top of that it offers all of that for older devices as well, which allows the app to use a lower minimum Sdk version. The same functionality is also provided by the open source re-implementation of Google Play Services called microG, where it is listed under "[Fused Location](https://github.com/microg/GmsCore/wiki/Implementation-Status)".

This is however still not the perfect solution and may be subject to change in the future.

#### osmdroid

To display the Map in the Android app, the osmdroid library has been used, which uses OpenStreetMaps.

#### Kotlin Serialization

To serialize data classes into JSON strings as well as to serialize ByteArrays to Base64 strings, Kotlin Serialization is used, to make the serialization and deserialization process as simple as possible.

### Unit tests

The Android app's unit tests ensure, that all the cryptographic functions are working as expected. They make sure that data that gets encrypted is actually able to be decrypted, that the same MAC is always generated for the same key and data etc.
