package findmydevicee2ee.android

import org.junit.Assert
import org.junit.Test
import javax.crypto.spec.SecretKeySpec

class CryptoTest {

    @Test
    fun encrypt_decrypt_random_key() {
        val input = "Hello world!"
        val key = genKey()

        val data = encrypt(input.toByteArray(), key)
        val output = String(decrypt(data, key))
        Assert.assertEquals(input, output)
    }

    @Test
    fun encrypt_decrypt_password() {
        val input = "Hello world!"
        val password = "password"
        val salt = genSalt()

        val data = encrypt(input.toByteArray(), SecretKeySpec(deriveKey(password, salt), "AES"))
        val output = String(decrypt(data, SecretKeySpec(deriveKey(password, salt), "AES")))
        Assert.assertEquals(input, output)
    }

    @Test
    fun sign_verify() {
        val input = "Hello world!"
        val keyPair = genKeyPair()

        val signature = sign(input.toByteArray(), keyPair.private)
        Assert.assertTrue(verify(input.toByteArray(), signature, keyPair.public))
    }

    @Test
    fun mac_test() {
        val input = "Hello world!"
        val key = genKey()

        val mac1 = mac(input.toByteArray(), key)
        val mac2 = mac(input.toByteArray(), key)
        Assert.assertArrayEquals(mac1, mac2)
    }

    @Test
    fun mac_sanity() {
        val input = "Hello world!"
        val key = genKey()

        val mac = mac(input.toByteArray(), key)
        Assert.assertNotEquals(mac, input)
    }

}