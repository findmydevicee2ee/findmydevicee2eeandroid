package findmydevicee2ee.android.activity

import android.Manifest
import android.content.pm.PackageManager
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.Priority
import findmydevicee2ee.android.R
import findmydevicee2ee.android.REQUEST_LOCATION_BACKGROUND
import findmydevicee2ee.android.REQUEST_LOCATION_FOREGROUND
import findmydevicee2ee.android.getAccountUuid
import findmydevicee2ee.android.getKey
import findmydevicee2ee.android.getUrl
import findmydevicee2ee.android.macAndEncrypt
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody
import org.osmdroid.config.Configuration.getInstance
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.util.BoundingBox
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.Marker

class MainActivity : AppCompatActivity() {

    private lateinit var map : MapView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION),
                REQUEST_LOCATION_FOREGROUND
            )
        } else {
            updateLocation()
        }

        //load/initialize the osmdroid configuration, this can be done
        // This won't work unless you have imported this: org.osmdroid.config.Configuration.*
        getInstance().load(this, PreferenceManager.getDefaultSharedPreferences(this))
        //setting this before the layout is inflated is a good idea
        //it 'should' ensure that the map has a writable location for the map cache, even without permissions
        //if no tiles are displayed, you can try overriding the cache path using Configuration.getInstance().setCachePath
        //see also StorageUtils
        //note, the load method also sets the HTTP User Agent to your application's package name, if you abuse osm's
        //tile servers will get you banned based on this string.

        //inflate and create the map
        setContentView(R.layout.activity_main)

        map = findViewById(R.id.map)
        map.setTileSource(TileSourceFactory.MAPNIK)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode == REQUEST_LOCATION_FOREGROUND && PackageManager.PERMISSION_DENIED !in grantResults) {
            updateLocation()
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.ACCESS_BACKGROUND_LOCATION),
                    REQUEST_LOCATION_BACKGROUND
                )
            }
        }
    }

    override fun onResume() {
        super.onResume()
        //this will refresh the osmdroid configuration on resuming.
        //if you make changes to the configuration, use
        //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        //Configuration.getInstance().load(this, PreferenceManager.getDefaultSharedPreferences(this));
        //map.onResume() //needed for compass, my location overlays, v6.0.0 and up
    }

    override fun onPause() {
        super.onPause()
        //this will refresh the osmdroid configuration on resuming.
        //if you make changes to the configuration, use
        //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        //Configuration.getInstance().save(this, prefs);
        //map.onPause()  //needed for compass, my location overlays, v6.0.0 and up
    }

    private fun updateLocation() {

        val fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        fusedLocationClient.lastLocation.addOnSuccessListener { location : Location? ->

            location ?: return@addOnSuccessListener

            Log.i("MainActivity", location.toString())
            val marker = Marker(map)
            val geoPoint = GeoPoint(location)
            marker.position = geoPoint
            marker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM)
            map.overlays.add(marker)

            var boundingBox = BoundingBox.fromGeoPoints(arrayOf(geoPoint).toMutableList())
            boundingBox = boundingBox.increaseByScale(0.00001f)
            map.zoomToBoundingBox(boundingBox, false)
        }

        val locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                val locations = ArrayList<findmydevicee2ee.android.Location>()
                for (location in locationResult.locations) {
                    Log.i("MainActivity", location.toString())
                    locations.add(
                        findmydevicee2ee.android.Location(
                            location.latitude,
                            location.longitude,
                            location.time
                        )
                    )
                }
                val key = getKey(this@MainActivity)
                val encryptedData = macAndEncrypt(Json.encodeToString(locations).toByteArray(), key)
                val encryptedJsonData = Json.encodeToString(encryptedData)

                Thread {
                    val response = OkHttpClient().newCall(
                        Request.Builder()
                            .url(getUrl(this@MainActivity) + "/" + getAccountUuid(this@MainActivity) + "/0")
                            .post(RequestBody.create(MediaType.parse("application/json"), encryptedJsonData))
                            .build()
                    ).execute()
                    if(!response.isSuccessful) {
                        //val file = File(cacheDir, "unsent.jsonl")
                        //file.appendText(encryptedJsonData + "\n")
                        //TODO regularly attempt to resend data
                    }
                }.start()
            }

        }

        val locationRequest = LocationRequest.Builder(5000)
            .setPriority(Priority.PRIORITY_HIGH_ACCURACY)
            .build()

        //fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper())
    }

}