package findmydevicee2ee.android.activity

import android.app.admin.DevicePolicyManager
import android.content.ComponentName
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import findmydevicee2ee.android.receiver.DeviceAdminReceiver
import findmydevicee2ee.android.R
import findmydevicee2ee.android.getPrefs
import info.guardianproject.netcipher.proxy.OrbotHelper
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody
import java.lang.Exception
import java.lang.IllegalArgumentException
import java.net.SocketTimeoutException

class SetupActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //TODO figure something out. This doesn't work.
        Log.i("SetupActivity", OrbotHelper.get(this).init().toString())

        if(getPrefs(this).getBoolean("setupComplete", false)) {
            startActivity(Intent(this, MainActivity::class.java))
            finish()
            return
        }

        val adminReceiverComponent = ComponentName(this, DeviceAdminReceiver::class.java)

        val intent = Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN).apply {
            putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, adminReceiverComponent)
            putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, getString(R.string.admin_prompt))
        }
        startActivity(intent)

        enableEdgeToEdge()
        setContentView(R.layout.activity_setup)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }

        val submit = findViewById<Button>(R.id.submit)
        submit.setOnClickListener {
            submit.isEnabled = false
            Thread {
                val emptyReqBody = RequestBody.create(null, byteArrayOf())
                var registrationFailed = true
                try {
                    val response = OkHttpClient().newCall(
                        Request.Builder()
                            .url(findViewById<EditText>(R.id.address).text.toString() + "/register")
                            .post(emptyReqBody)
                            .build()
                    ).execute()
                    if (response.code() == 200) {
                        registrationFailed = false
                        val intent = Intent(this, SaveBackupActivity::class.java)
                        intent.putExtra("accountUuid", response.body().string())
                            .putExtra("password", findViewById<EditText>(R.id.password).text.toString())
                            .putExtra("url", findViewById<EditText>(R.id.address).text.toString())
                        startActivity(intent)
                        finish()
                    }
                } catch(e : Exception) {
                    when(e) {
                        is IllegalArgumentException, is SocketTimeoutException -> {
                            // ignore
                        }
                        else -> throw e
                    }
                }

                runOnUiThread {
                    if(registrationFailed) {
                        Toast.makeText(
                            this,
                            "Device registration at the given address failed. Please make sure it is correct.",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                    submit.isEnabled = true
                }
            }.start()
        }
    }
}