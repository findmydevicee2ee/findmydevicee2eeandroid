package findmydevicee2ee.android.activity

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import findmydevicee2ee.android.R
import findmydevicee2ee.android.REQUEST_BACKUP_PATH
import findmydevicee2ee.android.deriveKey
import findmydevicee2ee.android.genBackupFile
import findmydevicee2ee.android.genSalt
import findmydevicee2ee.android.getPrefs
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.util.Base64
import javax.crypto.spec.SecretKeySpec

class SaveBackupActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_save_backup)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }

        val submit = findViewById<Button>(R.id.submit)
        submit.setOnClickListener {
            val createDocumentIntent = Intent(Intent.ACTION_CREATE_DOCUMENT)
                .putExtra(Intent.EXTRA_TITLE, "findmy.backup")
                .setType("*/*") // required by file picker
            startActivityForResult(createDocumentIntent, REQUEST_BACKUP_PATH)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == REQUEST_BACKUP_PATH && resultCode == RESULT_OK) {

            val accountUuid = intent.extras?.getString("accountUuid")!!
            val url = intent.extras?.getString("url")!!
            var password = intent.extras?.getString("password")!!

            // A placeholder value is required in case of an empty password.
            // Otherwise the key derivation is going to fail.
            password = if(password != "") password else "empty"

            val salt = genSalt()
            val key = deriveKey(password, salt)
            val backupFile = genBackupFile(accountUuid, SecretKeySpec(key, "AES"), salt)

            //TODO add missing error checks
            val outStream = contentResolver.openOutputStream(data!!.data!!)!!
            outStream.write(Json.encodeToString(backupFile).toByteArray())
            outStream.close()

            getPrefs(this).edit()
                .putString("url", url)
                .putString("accountUuid", accountUuid)
                .putString("key", Base64.getEncoder().encodeToString(deriveKey(password, backupFile.salt)))
                .apply()

            startActivity(Intent(this, PermissionActivity::class.java))
            finish()
        }
    }

}