package findmydevicee2ee.android

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import java.util.Base64
import javax.crypto.SecretKey
import javax.crypto.spec.SecretKeySpec

private var key : SecretKey? = null
private var accountUuid : String? = null
private var url : String? = null

fun getPrefs(context: Context) : SharedPreferences {
    return context.getSharedPreferences("prefs", AppCompatActivity.MODE_PRIVATE)
}

fun getAccountUuid(context: Context) : String? {
    if(accountUuid == null) {
        accountUuid = getPrefs(context).getString("accountUuid", null)
    }
    return accountUuid
}

fun getUrl(context: Context) : String? {
    if(url == null) {
        url = getPrefs(context).getString("url", null)
    }
    return url
}

// Make sure to only call this when the SharedPreference String "key" has actually been set!
fun getKey(context: Context) : SecretKey {
    if(key == null) {
        val keyStr = getPrefs(context).getString("key", null)
        val keyBin = Base64.getDecoder().decode(keyStr)
        key = SecretKeySpec(keyBin, "AES")
    }
    return key as SecretKey
}