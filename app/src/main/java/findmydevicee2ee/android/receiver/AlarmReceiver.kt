package findmydevicee2ee.android.receiver

import android.Manifest
import android.app.AlarmManager
import android.app.PendingIntent
import android.app.admin.DevicePolicyManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.SystemClock
import android.util.Log
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.LocationServices
import findmydevicee2ee.android.AuthenticatedData
import findmydevicee2ee.android.EncryptedData
import findmydevicee2ee.android.decrypt
import findmydevicee2ee.android.getAccountUuid
import findmydevicee2ee.android.getKey
import findmydevicee2ee.android.getUrl
import findmydevicee2ee.android.mac
import findmydevicee2ee.android.macAndEncrypt
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody
import java.lang.Exception

class AlarmReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent?) {
        fetchAndExecuteRemoteCommands(context)
        sendLocationData(context)
    }

    private fun fetchAndExecuteRemoteCommands(context: Context) {
        Thread {
            val response = OkHttpClient().newCall(
                Request.Builder()
                    .url(getUrl(context) + "/" + getAccountUuid(context) + "/1")
                    .build()
            ).execute()
            Log.i("alarm", "fetched commands")
            if(response.isSuccessful) {
                for(command in Json.decodeFromString<Array<EncryptedData>>(response.body().string())) {
                    Log.i("alarm", "processing command")
                    val key = getKey(context)
                    // use try catch to ensure that valid commands get processed even if the response contains invalid commands
                    try {
                        Log.i("alarm", "decoded command")
                        val decryptedCommand = decrypt(command, key)
                        Log.i("alarm", "decrypted command")
                        val authenticatedData =
                            Json.decodeFromString<AuthenticatedData>(decryptedCommand.toString(Charsets.UTF_8))
                        Log.i("alarm", "decoded decrypted command")
                        val mac = mac(authenticatedData.data, key)
                        Log.i("alarm", "created mac of command")
                        if (!authenticatedData.mac.contentEquals(mac)) {
                            Log.i("alarm", "invalid mac detected")
                            return@Thread //invalid command, ignore
                        }
                        Log.i("alarm", "mac validation successful!")
                        authenticatedData.data.toString()
                        Log.i("alarm", "command:" + authenticatedData.data.toString(Charsets.UTF_8))
                        when (authenticatedData.data.toString(Charsets.UTF_8)) {
                            "wipe" -> {
                                Log.i("alarm", "valid wipe command detected")
                                val dpm =
                                    context.getSystemService(Context.DEVICE_POLICY_SERVICE) as DevicePolicyManager
                                dpm.wipeData(DevicePolicyManager.WIPE_EXTERNAL_STORAGE)
                                Log.i("alarm", "wipe process completed")
                            }
                            else -> {}
                        }
                    } catch(e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
        }.start()
    }

    private fun sendLocationData(context: Context) {
        val fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)

        if (ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }

        fusedLocationClient.lastLocation.addOnSuccessListener { location : Location? ->

            location ?: return@addOnSuccessListener

            val locations = ArrayList<findmydevicee2ee.android.Location>()
            Log.i("activity", location.toString())
            locations.add(
                findmydevicee2ee.android.Location(
                    location.latitude,
                    location.longitude,
                    location.time
                )
            )
            val key = getKey(context)
            val encryptedData = macAndEncrypt(Json.encodeToString(locations).toByteArray(), key)
            val encryptedJsonData = Json.encodeToString(encryptedData)

            Thread {
                val response = OkHttpClient().newCall(
                    Request.Builder()
                        .url(getUrl(context) + "/" + getAccountUuid(context) + "/0")
                        .post(RequestBody.create(MediaType.parse("application/json"), encryptedJsonData))
                        .build()
                ).execute()
                if(!response.isSuccessful) {
                    //val file = File(context.cacheDir, "unsent.jsonl")
                    //file.appendText(encryptedJsonData + "\n")
                    //TODO regularly attempt to resend data
                }
            }.start()

        }
    }

}

fun setupAlarm(context: Context) {
    val intent = Intent(context, AlarmReceiver::class.java)
    val pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_IMMUTABLE)
    val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
    alarmManager.setInexactRepeating(
        AlarmManager.ELAPSED_REALTIME,
        SystemClock.elapsedRealtime() + 1000 * 60,
        AlarmManager.INTERVAL_HOUR,
        pendingIntent);
}
