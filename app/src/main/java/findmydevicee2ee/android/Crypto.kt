package findmydevicee2ee.android

import kotlinx.serialization.Serializable
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.security.KeyPair
import java.security.KeyPairGenerator
import java.security.PrivateKey
import java.security.PublicKey
import java.security.SecureRandom
import java.security.Signature
import javax.crypto.Cipher
import javax.crypto.KeyGenerator
import javax.crypto.Mac
import javax.crypto.SecretKey
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.PBEKeySpec

@Serializable
data class AuthenticatedData(
    @Serializable(with = ByteArrayAsBase64Serializer::class)
    val data: ByteArray,
    @Serializable(with = ByteArrayAsBase64Serializer::class)
    val mac: ByteArray,
    val algorithm: String
)

@Serializable
data class EncryptedData(
    @Serializable(with = ByteArrayAsBase64Serializer::class)
    val data: ByteArray,
    val transformation: String,
    @Serializable(with = ByteArrayAsBase64Serializer::class)
    val iv: ByteArray
)

@Serializable
data class BackupFile(
    val encryptedData: EncryptedData,
    @Serializable(with = ByteArrayAsBase64Serializer::class)
    val salt: ByteArray,
    val algorithm: String,
    val iterationCount: Int,
    val keyLengthInBits: Int
)

const val KEY_DERIVATION_ALGORITHM = "PBKDF2withHmacSHA256"
const val KEY_DERIVATION_ITERATIONS = 600_000
const val KEY_DERIVATION_KEY_LENGTH_IN_BITS = 256
const val MAC_ALGORITHM = "HmacSHA256"
const val ENCRYPTION_TRANSFORMATION = "AES/CBC/PKCS5PADDING"

fun genKeyPair() : KeyPair {
    val keyPairGen = KeyPairGenerator.getInstance("RSA")
    keyPairGen.initialize(4096)
    return keyPairGen.genKeyPair()
}

fun genKey() : SecretKey {
    val keygen = KeyGenerator.getInstance("AES")
    keygen.init(256)
    return keygen.generateKey()
}

fun sign(data: ByteArray, privateKey: PrivateKey) : ByteArray {
    val signature = Signature.getInstance("SHA512withRSA")
    signature.initSign(privateKey)
    signature.update(data)
    return signature.sign()
}

fun verify(data: ByteArray, signatureBytes: ByteArray, publicKey: PublicKey) : Boolean {
    val signature = Signature.getInstance("SHA512withRSA")
    signature.initVerify(publicKey)
    signature.update(data)
    return signature.verify(signatureBytes)
}

//Note: a random IV is chosen by default
fun encrypt(data: ByteArray, secretKey: SecretKey) : EncryptedData {
    val transformation = ENCRYPTION_TRANSFORMATION
    val cipher = Cipher.getInstance(transformation)
    cipher.init(Cipher.ENCRYPT_MODE, secretKey)
    val ciphertext: ByteArray = cipher.doFinal(data)
    return EncryptedData(ciphertext, transformation, cipher.iv)
}

fun decrypt(data: EncryptedData, secretKey: SecretKey) : ByteArray {
    val cipher = Cipher.getInstance(data.transformation)
    cipher.init(Cipher.DECRYPT_MODE, secretKey, IvParameterSpec(data.iv))
    return cipher.doFinal(data.data)
}

fun mac(data: ByteArray, key: SecretKey) : ByteArray {
    val mac = Mac.getInstance(MAC_ALGORITHM)
    mac.init(key)
    return mac.doFinal(data)
}

fun deriveKey(password: String, salt: ByteArray) : ByteArray {
    // iteration recommendation: https://cheatsheetseries.owasp.org/cheatsheets/Password_Storage_Cheat_Sheet.html#pbkdf2
    val spec = PBEKeySpec(password.toCharArray(), salt,  KEY_DERIVATION_ITERATIONS, KEY_DERIVATION_KEY_LENGTH_IN_BITS)
    val factory = SecretKeyFactory.getInstance(KEY_DERIVATION_ALGORITHM)
    return factory.generateSecret(spec).encoded
}

fun genSalt() : ByteArray {
    val salt = ByteArray(64) // 512 bits
    SecureRandom().nextBytes(salt)
    return salt
}

fun genBackupFile(accountUuid: String, key: SecretKey, salt: ByteArray) : BackupFile {
    val encryptedData = macAndEncrypt(accountUuid.toByteArray(), key)
    return BackupFile(encryptedData, salt, KEY_DERIVATION_ALGORITHM, KEY_DERIVATION_ITERATIONS, KEY_DERIVATION_KEY_LENGTH_IN_BITS)
}

fun macAndEncrypt(data: ByteArray, key: SecretKey) : EncryptedData {
    val mac = mac(data, key)
    return encrypt(Json.encodeToString(AuthenticatedData(data, mac, MAC_ALGORITHM)).toByteArray(), key)
}
